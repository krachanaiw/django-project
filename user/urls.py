
from django.urls import path

from user.views import base

app_name = "user"

urlpatterns = [

    path('', base, name="base"),
]